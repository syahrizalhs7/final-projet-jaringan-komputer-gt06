#include <winsock2.h>
#include <stdio.h>

int WSAStartup(
  _In_  WORD      wVersionRequested,
  _Out_ LPWSADATA lpWSAData
);
WORD winsock_version = 0x202;
WSADATA winsock_data;
if( WSAStartup( winsock_version, &winsock_data ) )
{
	printf( "WSAStartup failed: %d", WSAGetLastError() );
	return;
}

int recvfrom(
_In_        SOCKET          s,
_Out_       char            *buf,
_In_        int             len,
_In_        int             flags,
_Out_       struct sockaddr *from,
_Inout_opt_ int             *fromlen
);

har buffer[SOCKET_BUFFER_SIZE];
int flags = 0;
SOCKADDR_IN from;
int from_size = sizeof( from );
int bytes_received = recvfrom( sock, buffer, SOCKET_BUFFER_SIZE, flags, (SOCKADDR*)&from, &from_size );

if( bytes_received == SOCKET_ERROR )
{
   printf( "recvfrom returned SOCKET_ERROR, WSAGetLastError() %d", WSAGetLastError() );
}
else
{
   buffer[bytes_received] = 0;
   printf( "%d.%d.%d.%d:%d - %s", 
   from.sin_addr.S_un.S_un_b.s_b1, 
   from.sin_addr.S_un.S_un_b.s_b2, 
   from.sin_addr.S_un.S_un_b.s_b3, 
   from.sin_addr.S_un.S_un_b.s_b4, 
   from.sin_port, 
   buffer );
}
