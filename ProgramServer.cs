﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {

                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                //opening welcome
                string name = reader.ReadLine();
                Console.WriteLine("Welcome " + name + " to this game!");
                writer.WriteLine("------------------------------------------------------");
                writer.Flush();
                writer.WriteLine("Fill the blank with the next number!");
                writer.Flush();
                string a = reader.ReadLine();
                int b;
                int.TryParse(a, out b);

                int skor = 0;
                int n = 0;
                //game question

                for (int i = 0; i < b; i++)
                {
                    String opr = String.Empty;
                    String question1 = String.Empty;
                    int questionscore = 0;
                    int questionscore1 = 0;
                    Random rndm = new Random();
                    var rand_question1 = rndm.Next(1, 6);
                    var rand_opr = rndm.Next(1, 5);
                    switch (rand_question1)
                    {
                        case 1:
                            question1 = " 4, 12, 28, 60, 124, 252, ...";
                            questionscore1 = 508;
                            break;
                        case 2:
                            question1 = "½, 1, 1, 3, 9, 13, 65, 71, ...";
                            questionscore1 = 497;
                            break;
                        case 3:
                            question1 = "4, 5, 11, 13, 25, 29, 46, ...";
                            questionscore1 = 53;
                            break;
                        case 4:
                            question1 = "5, 8, 13, 21, 34, 55, 89, 144, ...";
                            questionscore1 = 233;
                            break;
                        case 5:
                            question1 = "3, 6, 6, 10, 10, 14, 15, ...";
                            questionscore1 = 18;
                            break;
                        default:
                            break;
                    }
                    Console.WriteLine(" ");
                    string answerstring;
                    int answer;
                    Console.WriteLine("Question for " + name);
                    writer.WriteLine(question1);
                    writer.Flush();
                    answerstring = reader.ReadLine();
                    int.TryParse(answerstring, out answer);
                    Console.WriteLine(name + " = " + answer);
                    if (answer == questionscore1)
                    {
                        Console.WriteLine(name + " answer is CORRECT");
                        writer.WriteLine("Your answer is CORRECT");
                        writer.Flush();
                        n++;
                    }
                    else
                    {
                        Console.WriteLine(name + " answer is WRONG");
                        writer.WriteLine("Your answer is WRONG");
                        writer.Flush();
                    }
                      
                }
                skor = 100 * n / b;
                Console.WriteLine(" ");
                writer.WriteLine("Your Score is " + skor);
                writer.Flush();
                writer.WriteLine("Thank you for playing this game");
                writer.Flush();
                writer.Write("Press Any Key . . .");
                writer.Flush();
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine(name + " Disconnected . . .");
            }
            catch (IOException)
            {
                Console.WriteLine("Disconnected . . .");
                Console.WriteLine(" ");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }

        }
        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                int i = 0;
                string playerString;
                int player = 0;
                listener = new TcpListener(IPAddress.Parse("192.168.43.200"), 12000);
                listener.Start();
                Console.WriteLine("Server Started!");
                Console.Write("How many player do you want to join the game?");
                playerString = Console.ReadLine();
                int.TryParse(playerString, out player);
                Console.WriteLine("Waiting for player . . .");
                while (i < player)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                    player++;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}
