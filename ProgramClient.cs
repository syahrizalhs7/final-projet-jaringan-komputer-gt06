﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            string ipAdd;
            string portstring;
            int port;
            Console.WriteLine("Which IP Address and port do you want to connect?");
            Console.Write("IP Address = ");
            ipAdd = Console.ReadLine();
            Console.Write("Port = ");
            portstring = Console.ReadLine();
            int.TryParse(portstring, out port);
            TcpClient client = new TcpClient(ipAdd, port);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());

            String name = String.Empty;
            Console.WriteLine("=====================================");
            Console.Write("Enter Your Name = ");
            name = Console.ReadLine();
            writer.WriteLine(name);
            writer.Flush();
            Console.WriteLine(" ");
            for (int i = 0; i < 2; i++)
            {
                string welcome = reader.ReadLine();
                Console.WriteLine(welcome);
            }

            Console.WriteLine(" ");
            Console.WriteLine("How many question do u want? (max=5)");
            Console.WriteLine(" ");

            int b;
            string a;
            a = Console.ReadLine();
            int.TryParse(a, out b);
            writer.WriteLine(a);
            writer.Flush();

            for (int i = 0; i < b; i++)
            {
                string soal;
                string answer;
                soal = reader.ReadLine();
                Console.Write(soal);
                answer = Console.ReadLine();
                writer.WriteLine(answer);
                writer.Flush();
                string n = reader.ReadLine();
                Console.WriteLine(n);
            }
            Console.WriteLine(" ");

            string closing;

            for (int i = 0; i < 2; i++)
            {
                closing = reader.ReadLine();
                Console.WriteLine(closing);
            }


            for (int i = 0; i < 2; i++)
            {
                closing = reader.ReadLine();
                Console.WriteLine(closing);
            }
            Console.WriteLine("=====================================");
            closing = reader.ReadLine();
            Console.Write(closing);
            Console.ReadKey();
            reader.Close();
            writer.Close();
            client.Close();

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}
